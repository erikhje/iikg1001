# OpenStack Orchestration Lab exercises

Note: Everything in these labs (including your access to SkyHiGh) will be revoked/deleted on Nov 10th!

## Virtuell Windows i skyen

Merk: hvis du ikke er i et NTNU-nett, så bruk [VPN](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Installere+VPN).

1. Logg inn på <https://skyhigh.iik.ntnu.no> (NTNU brukernavn og
passord)
1. Lag et nøkkelpar:
   1. Compute
   1. Key Pairs
   1. Create Key Pair
   1. Key Pair Name `mykey`
   1. Create Key Pair (**ta godt vare på filen mykey.pem som du da
				får som output, det er din private nøkkel som du trenger
				senere**)
1. Lag en Windows server i et nettverk:
   1. Orchestration
   1. Stacks
   1. Launch Stack
   1. Template source: URL
   1. Template URL:  
   `https://folk.ntnu.no/erikhje/single_windows_instance.yaml`
   1. (You do not have to choose “Environment Source” or
				“Environment File”)
   1. Stack Name: `wMysil` (eller hva du vil)
   1. Skriv inn NTNU-passordet ditt
   1. Sjekk at key\_name sier `mykey`
   1. Launch
1. (vent ca 15 minutter)
1. Brukernavnet ditt er `admin`. Finn ut hva passordet ditt er:
   1. Compute
   1. Instances
   1. Retrieve Password (fra nedtrekksmeny til høyre)
   1. Private Key File: last opp `mykey.pem`
   1. Decrypt Password
   1. Kopier og ta vare på passordet du ser i feltet Password
1. Noter deg hvilken Floating IP servern din har (Compute,
						Instances), det er denne IP-adressen du skal benytte for å koble
		deg til servern med Remote Desktop.
1. For å koble deg opp mot den nylagde Windows-maskina så benytt en
eller annen Remote Desktop klient, f.eks. `mstsc` på Windows,
		`Microsoft Remote Desktop connection` fra App store på Mac,
		eller på Linux `apt install freerdp2-x11` og  
		`xfreerdp /u:Admin +clipboard /w:1366 /h:768 /v:SERVER_IP`
1. Before you start using a new server, better make sure all
software is updated:
```powershell
# PowerShell -> Run as administrator 
Set-ExecutionPolicy RemoteSigned
# Oppdater
Install-Module -Name PSWindowsUpdate
Get-WUInstall -Verbose
Get-WUInstall -Install
```

## Virtual Linux in a private cloud

1. Log in to
<https://skyhigh.iik.ntnu.no>
(NTNU username and
password)
2. Create a Linux server in a network:
   1. Orchestration
   2. Stacks
   3. Launch Stack
   4. Template source: URL
   5. Template URL:  
`https://folk.ntnu.no/erikhje/single_linux_instance.yaml`
   6. (You do not have to choose “Environment Source” or
				“Environment File”)
   7. Stack Name: `Mysil` (or whatever you prefer)
   8. Enter your NTNU-password
   9. Check that key\_name is `mykey`
   10. Launch
3. (wait ca one minute)
4. Make a note of what Floating IP your server has received
(Compute, Instances), this is the IP address you need to use
when connecting to your server with ssh.
5. Your username is `ubuntu`. There is no password, use your
private key (the file `mykey.pem`) to log in:

`ssh -i mykey.pem ubuntu@FLOATING_IP_ADDRESS`

If on Mac or Linux, you probably need to do `chmod 600
mykey.pem` first.

On Windows, you probably need to do in PowerShell

```
cmd
icacls .\mykey.pem /inheritance:r
icacls .\mykey.pem /grant:r "%username%":"(R)"
exit
```

6. Before you start using a new server, better make sure all
software is updated:
```bash
sudo apt update
sudo apt dist-upgrade
sudo reboot # if new Linux kernel installed, but do it anyway
```
