# Linux CLI and Computer Networks

Note: compendia.md is a version of compendia.pdf with the only purpose of making it easier for students to copy and paste, so

* Read compendia.pdf
* copy and paste text from compendia.md when needed (since copying from PDF-files is error prone)
