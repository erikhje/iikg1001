[[_TOC_]]

## About this Compendium

This is NOT A TEXTBOOK. This a just a collection of lecture notes
together with weekly exercises and labs.

Special thanks go out to the following individuals who provided valuable
feedback: Mariusz Nowostawski, Christopher Frantz, Ivar Farup, Ernst
Gunnar Gran, Eigil Obrestad, Donn Morrison and Slobodan Petrovic.

# Login, Files and Directories

## What Is the Shell

  - Shell

  - Terminal

  - Prompt

  - date, cal, df, free

  - History  
    arrow up and down, Also TAB-completion and CTRL-r

## Navigation

  - Root directory

  - Current working directory

  - Parent directory

  - Home directory

  - pwd, ls, cd

  - Absolute vs Relative path

  - cd . .. - 

  - ls -a

## Exploring the System

  - Command Option(s) Argument(s)

  - ls -ltr 

  - file, less

  - Configuration file

  - Script

  - File System: home, root, etc, bin, tmp, media

  - Symbolic link

## Manipulating Files and Directories

  - Wildcards: \* ?

  - cp, mkdir, mv, rm, ln -s  
    (you can skip the topic “Hard links”)

  - The editor vi: insert/command mode

## About the Lab Exercises

Note: text in `UPPER_CASE` should be replaced by you when you type the
example commands.

### If not a student at NTNU

![image](img/net-setup-fig-nonNTNU.png)

### If you are a student at NTNU

![image](img/net-setup-fig.png)

## Review questions and problems

1.  What is the relationship between a Shell and a Terminal?
    
    1.  A Terminal and a Shell is the same thing.
    
    2.  A Terminal interprets commands and a Shell is a program we use
        to get access to the Terminal.
    
    3.  A Terminal is what we use to terminate a Shell.
    
    4.  A Shell interprets commands and a Terminal is a program we use
        to get access to the Shell.

2.  Which one of the following examples of file paths is an absolute
    pathname?
    
    1.  `./work/courses`
    
    2.  `work/courses`
    
    3.  `/home/ubuntu/work/courses`
    
    4.  `../ubuntu/work/courses`

3.  How do you create a symbolic link from `/var/www/html` to your
    current working directory?
    
    1.  `ln -s /var/www/html .`
    
    2.  `ln /var/www/html .`
    
    3.  `ln /var/www/html /.`
    
    4.  `ln -s /var/www/html ~`

4.  Copy the file `/etc/passwd` to your home directory. Rename it to
    `mypasswd`

5.  Open the file `mypasswd` with `vi`. Delete the first five lines of
    the file, save and quit.

6.  Create a directory `backups`. Copy all files from another directory
    to the `backups` directory by copying recursively and preserving all
    time stamps.

## Lab tutorials

1.  **Log in to a Linux server**. If you use Mac or Linux you probably
    already have ssh installed, so just open a terminal. If you use
    Windows, do the following: right click on the start button, click
    Windows PowerShell(Admin). When in PowerShell:
    
        Get-WindowsCapability -Online | ? Name -like 'OpenSSH*'
        
        # This should return which versions are available, install client:
        
        Add-WindowsCapability -Online -Name OpenSSH.Client
        
        # When install has completed successfully, exit PowerShell
    
    *IF NOT A NTNU-STUDENT: replace `YOUR_USERNAME@login.stud.ntnu.no`
    in the text below with the username and IP-address for your local
    virtual machine. You should try to log in to your local virtual
    machine both by ssh and by opening it as an application.*
    
    Right click on the start button, click Windows PowerShell. When in
    PowerShell (or in a terminal on Mac or Linux): `ssh
    YOUR_USERNAME@login.stud.ntnu.no`
    
    Note: when you have ssh, you also have scp for copying files, just
    remember to add `:` at the end of the command, e.g. if I have a
    local file `mysil.txt` I want to copy to the server:  
    `scp mysil.txt YOUR_USERNAME@login.stud.ntnu.no:`

2.  **Commands and command line history**.
    
    1.  Try all the commands in chapter one of .
    
    2.  Use the up and down arrows to see how you can repeat commands
        from your history.
    
    3.  Do  
        `cat .bash_history`  
        log out and log back in again, and repeat  
        `cat .bash_history`
    
    4.  Google to try and find out the default maximum number of
        commands saved into the `.bash_history` file.

3.  **Navigation**.
    
    1.  Try all the commands in chapter two of .

4.  **Exploring the system.**.
    
    1.  Try all the commands in chapter three of . You do not have to
        know the entire file system hierarchy in table 3.4 but know that
        you can look it up when needed. Also try to use TAB completion
        to quickly do the following commands  
        
            cd /usr/local/share/ca-certificates/
            ls -l /var/lib/ubuntu-release-upgrader/release-upgrade-available
            cd /usr/share/perl-openssl-defaults/
            cd /usr/include/openmpi/pmix/src/include
    
    2.  Make sure you understand how to search in a text file that you
        are viewing with `less` (`/mysil` will search for the text
        `mysil` and `n` will show you the next occurrence). This is
        useful because this is how it works in a couple of other widely
        used text editors and viewers as well.
    
    3.  Note: find out how you can copy and paste. This works in
        different ways dependent on which terminal you are using and if
        you are using Linux, Mac or Windows. Ask teacher if unsure.
    
    4.  Do `ls -l` in your home directory. Do you have any symbolic
        links there?

5.  **Manipulating files and directories**.
    
    1.  Try all the commands in chapter four of .
    
    2.  Do the following
        
            cd
            cp /etc/passwd .
            ls -l passwd
            cp -a /etc/passwd .
            ls -l passwd
        
        What does the -a option do to the cp command?
    
    3.  In chapter five of , read carefully (and do the commands) in the
        section about `man`

6.  **Editing a file with vi**.
    
    1.  Read and do all the commands on pages 141-145 (the first five
        pages) in chapter 12 of .

7.  **Keep your session: Screen and tmux**. `screen` is the old classic
    “terminal multiplexer” while `tmux` is a newer version with some
    more features. If you know the basics of one of these, you can keep
    your session (everything you are currently doing) even if you log
    out or lose your network connection.
    
    1.  Log in to the server (replace this command if not a
        NTNU-student)  
        `ssh YOUR_USERNAME@login.stud.ntnu.no`
    
    2.  Start a tmux session and run a command in it  
        `tmux`  
        `ls`
    
    3.  Leave the tmux session by typing CTRL-b then d (for detach).
    
    4.  Log out of the server  
        `exit`
    
    5.  Log in to the server (replace this command if not a
        NTNU-student)  
        `ssh YOUR_USERNAME@login.stud.ntnu.no`
    
    6.  Go back into your tmux session  
        `tmux a`
    
    7.  See how you can do other things, e.g. create multiple windows
        and scroll up and down at <https://tmuxcheatsheet.com/>.

# Redirections and Expansions

## Redirection

  - Two types of output

  - STDIN, STDOUT, STDERR

  - Create, Append, Read  
    `>, >>, <`

  - STDERR to STDOUT  
    `2>&1`

  - /dev/null

  - cat

  - CTRL-d

  - Pipelines

  - Important difference  
    `| vs >`

  - Filters: sort, uniq, wc, grep

  - grep -irv

  - head/tail -n -f

  - tee

## Expansion

  - Expansion  
    “Each time we type a command and press the Enter key, bash performs
    several substitutions upon the text before it carries out our
    command. We have seen a couple of cases of how a simple character
    sequence, for example `*`, can have a lot of meaning to the shell.
    The process that makes this happen is called expansion” (, page 68).

  - $((2+2))

  - {A,B,C}, {01..12}

  - printenv

  - $( )

  - VARIABLES

  - Double quotes

  - Single quotes

  - Escaping a character

## Review questions and problems

1.  Only one of these makes sense, which one?
    
    1.  `$ command | file`
    
    2.  `$ file > command`
    
    3.  `$ command > file`
    
    4.  `$ file | command`

2.  What is the output of the command `echo "$(2 + 2)"` ?
    
    1.  `4`
    
    2.  `2 + 2`
    
    3.  `$(2 + 2)`
    
    4.  `2: command not found`

3.  Which character to you put in front of `$` if you want `echo "$a"`
    to actually print `$a` (and not expand it to the variable `a`) ?
    
    1.  `\`
    
    2.  `` ` ``
    
    3.  `|`
    
    4.  `/`

4.  Create the following directory structure using `mkdir -p` and brace
    expansion.
    
        .
        |--A
        |  |--0
        |  |--1
        |
        |--B
        |  |--0
        |  |--1
        |
        |--C
           |--0
           |--1
    
    Verify that it has been created with the `ls` command by using the
    option for recursive listing. Delete the entire directory structure
    you created with a single command.

5.  Write a command pipeline to list files in a directory hierarchy
    (e.g. your home directory) that have the name pattern `*.txt` and
    sort them by filesize.

6.  Write a command pipeline to recursively search for the word
    `hostname` in `/etc`. You should redirect standard error to
    `/dev/null` so you don’t see any "Permission denied" messages.

7.  Write a command to show the first five lines of the file
    `~/.bashrc`.

8.  Write a command pipeline to write lines 21-24 of `~/.bashrc` to a
    new file `strange.tmp`.

## Lab tutorials

1.  **Redirection and pipelines**.
    
    1.  Try all the commands in chapter six of .
    
    2.  What can you do with command `grep -ir mysil ~`

2.  **Expansion.**
    
    1.  Try all the commands in chapter seven of . On page 76, remember
        that you can create a file like this  
        `echo Mysil > "two words.txt"`
    
    2.  Do `man bash` and search in this man-page for the headline
        “ARITHMETIC EVALUATION”, and try the following commands
        
            i=5
            ((i++))
            echo $i
    
    3.  What was the `i` ? It was a variable, note that you need to
        avoid spaces when assigning variables otherwise Bash will
        interpret it as a command with options/arguments. Try the
        following commands
        
            a = Mysil
            echo $a
            a=Mysil
            echo $a
            a=$(pwd)
            echo $a

3.  **Extracting a number from a table**. We can show lines matching
    patterns with `grep`. What if we want to show only the third column
    from the lines? Try the following commands
    
        grep cpu0 /proc/stat
        grep cpu0 /proc/stat | awk '{print $3}'
        num=$(grep cpu0 /proc/stat | awk '{print $3}')
        echo $num
        ((num++))
        echo $num
    
    Note the command `awk '{print $3}'` to show column three.

# Permissions and Processes

## Permissions

  - Ownership  
    user, group, everyone

  - id

  - rwxrwxrwx

  - chmod

  - Number systems  
    binary (01), octal (0..7), decimal (0..9), hexadecimal (0..9A..F)

  - SetUID

  - sudo -s vs -i  
    (Note: Ubuntu disables logins to the root account)

  - chown, chgrp

  - adduser

## Processes

  - Multitasking and multiuser

  - Process ID (PID)

  - Process state

  - ps aux, top, htop

  - background  
    `nano &` or `nano`, CTRL-z, `bg`

  - foreground  
    `fg`

  - jobs

  - CTRL-c

  - kill

  - reboot, shutdown

  - pstree  
    (check out `tree` for files as well)

  - PATH  
    When you run a command, if it’s not a Bash builtin (like `cd`), Bash
    will look for the program in the directories listed in the
    environment variable PATH, and execute the first matching program it
    encounters.

## Review questions and problems

1.  Connect the commands in each column to the correct keyword.
    
    |  | `chmod` | `ps` | `ln -s` | `sudo` | `kill` | `id` |
    | :- | :------ | :--- | :------ | :----- | :----- | :--- |
    |  |         |      |         |        |        |      |
    |  |         |      |         |        |        |      |
    |  |         |      |         |        |        |      |
    |  |         |      |         |        |        |      |
    |  |         |      |         |        |        |      |
    |  |         |      |         |        |        |      |
    

2.  Connect the octal number in each column to the correct file
    permissions.
    
    |  | `640` | `777` | `700` | `755` | `600` | `444` |
    | :- | :---- | :---- | :---- | :---- | :---- | :---- |
    |  |       |       |       |       |       |       |
    |  |       |       |       |       |       |       |
    |  |       |       |       |       |       |       |
    |  |       |       |       |       |       |       |
    |  |       |       |       |       |       |       |
    |  |       |       |       |       |       |       |
    

3.  Write a command to show the line containing your username in
    `/etc/passwd` (note: this doesn’t work on *login* since that server
    is configured to use centralized user accounts instead of the local
    ones in `/etc/passwd`). Add a pipeline to the command to show only
    the fifth column where columns are separated with `:` (hint: `man
    cut`).

4.  Write commands for the following
    
    1.  Create a file `a.txt`
    
    2.  Change permissions on `a.txt` to `r--r-----`
    
    3.  Try to delete `a.txt` (redo steps above if you are able to
        delete it)
    
    4.  Change permissions on `a.txt` to `rw-------`
    
    5.  Add a user `mysil`
    
    6.  Change the owner of `a.txt` to be `mysil`
    
    7.  Try to delete `a.txt` (redo steps above if you are able to
        delete it)
    
    8.  Move `a.txt` to `/tmp/`
    
    9.  Try to delete `/tmp/a.txt`

5.  Write a command pipeline to show the three processes owned by you
    that uses the most RAM/physical memory (the RSS column you get from
    the `ps` command).

6.  Start a process in the background. Remove it with the `kill`
    command.

## Lab tutorials

1.  **(Skip this if you are not a NTNU-student) Virtual Linux in a
    private cloud**. Note: if you are not on a NTNU network, use
    [VPN](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Installere+VPN). We
    will now add a host to our setup so that we have two hosts in
    different subnets of the NTNU network. The host we have used until
    now we will refer to as *login* and the new host which we will
    create we will refer to as *VM* (short for Virtual Machine).
    
    1.  Log in to <https://skyhigh.iik.ntnu.no> (NTNU username and
        password)
    
    2.  Create a key pair:
        
        1.  Compute
        
        2.  Key Pairs
        
        3.  Create Key Pair
        
        4.  Key Pair Name `mykey`
        
        5.  Create Key Pair (**take special care of the file mykey.pem
            which will be outputted to you, this is your private key
            which you will need for login to your server soon**)
    
    3.  Create a Linux server in a network:
        
        1.  Orchestration
        
        2.  Stacks
        
        3.  Launch Stack
        
        4.  Template source: URL
        
        5.  Template URL:  
            `https://folk.ntnu.no/erikhje/single_linux_instance.yaml`
        
        6.  (You do not have to choose “Environment Source” or
            “Environment File”)
        
        7.  Stack Name: `Mysil` (or whatever you prefer)
        
        8.  Enter your NTNU-password
        
        9.  Check that key\_name is `mykey`
        
        10. Launch
    
    4.  (wait ca one minute)
    
    5.  Make a note of what Floating IP your server has received
        (Compute, Instances), this is the IP address you need to use
        when connecting to your server with ssh.
    
    6.  Your username is `ubuntu`. There is no password, use your
        private key (the file `mykey.pem`) to log in:
        
            ssh -i mykey.pem ubuntu@FLOATING_IP_ADDRESS
        
        (if on Mac or Linux, you probably need to do `chmod 600
        mykey.pem` first)
    
    7.  Before you start using a new server, better make sure all
        software is updated:
        
            sudo apt update
            sudo apt dist-upgrade
            sudo reboot # if new Linux kernel installed, but do it anyway

2.  **Permissions**.
    
    1.  Try all the commands in chapter nine of . Some commands (like
        `id`) you should try on both *login* and *VM*. Some of the
        commands for changing ownership and permissions might involve
        several user accounts. On *VM*, you have root access and can
        create new accounts, e.g.  
        `sudo adduser mysil`
    
    2.  See if any of the programs available to you have the SetUID-bit
        set  
        `ls -l /usr/bin | grep rws`

3.  **Processes**.
    
    1.  Try all the commands in chapter ten of . When you log in from
        remote, you will probably not be able to run GUI-programs like
        `xlogo` so replace `xlogo` with `nano` in the examples.
    
    2.  Do `pstree -p | less` Why isn’t `init` PID 1 like the book says?
        Google and/or ask teacher.

4.  **The environment variable PATH**.
    
    1.  Do `echo $PATH` on *VM*, then do
        
            VM$ mkdir ~/bin
            VM$ cat > ~/bin/ls
            #!/bin/bash
            echo "I love seeing files and directories!" 
            /bin/ls
            CTRL-d
            VM$ chmod +x ~/bin/ls
        
        log out and back in again, run `ls`, what happens and why?
    
    *Remember to always test your code. Linux commands and Bash are very
    powerful and it is easy to make mistakes. Make a note of the
    following references for future use:
    [ShellCheck](https://www.shellcheck.net/) and [Bash
    Pitfalls](https://mywiki.wooledge.org/BashPitfalls).*

# Computer Networks Introduction

## Internet, Edge and Core, Network of Networks

  - ISP  
    Internet Service Provider

  - TCP/IP

  - Protocol  
    A protocol defines the format and the order of messages exchanged
    between two or more communicating entities, as well as the actions
    taken on the transmission and/or receipt of a message

  - End system/Host

  - Access networks

  - Guided/unguided media

  - TP, Coax, Fiber optics

  - Packet switching

  - Routers and link-layer switches

  - Store-and-forward

  - Queuing delay and packet loss

  - Network of networks  
    Figure 1.15

  - Time scale  
    ms, milliseconds (one second is 1 000 (\(10^3\)) ms)  
    us, microseconds (one second is 1 000 000 (\(10^6\)) us)  
    ns, nanoseconds (one second is 1 000 000 000 (\(10^9\)) ns)

  - Processing delay

  - Queuing delay

  - Transmission delay

  - Propagation delay

  - Throughput  
    is what you see when downloading something (e.g. downloading at 3.32
    MB/s), but note that this is different from delay

  - Internet protocol stack  
    Application, Transport, Network, Link, Physical

  - Encapsulation

  - General threats  
    Malware, DoS, Packet sniffing, Masquerading

![image](img/hourglass.jpg)

<span>
<http://if-we.clients.labzero.com/code/posts/what-title-ii-means-for-tcp/>
</span>

## Review questions and problems

1.  An “end system” is the same as a “host”. These are commonly divided
    into two categories. Which categories?
    
    1.  TCP and IP.
    
    2.  Client and server.
    
    3.  Linux and Windows.
    
    4.  Mobile and PC.

2.  What does the Internet consist of? Choose five of these.
    
    1.  IXP
    
    2.  Tier 1 ISP
    
    3.  SAP
    
    4.  Tired ISP
    
    5.  Regional ISP
    
    6.  Multihome ASP
    
    7.  Content provider networks
    
    8.  Access ISP
    
    9.  Ethernet
    
    10. Sockets

3.  For each node (router or switch) in a network, there is four kinds
    of delay. *Queuing delay* is
    
    1.  Examining the packet’s header and determine where it should go.
    
    2.  The actual travel of a packet over a distance.
    
    3.  Pushing the entire packet into a link.
    
    4.  Waiting to be transmitted on a link.

4.  Ethernet and WiFi are examples of protocols in which layer of the
    Internet protocol stack?
    
    1.  Network.
    
    2.  Transport.
    
    3.  Link.
    
    4.  Application.

## Lab tutorials

1.  **Delay in networks**. It is nice to have a gut feeling of how much
    delay there actually is in computer networks. Most of us are
    involved in distributed applications where user satisfaction might
    be dependent on geographical placement of servers. It really does
    matter if important servers are located in a local region or across
    the globe if there is frequent communication.
    
    1.  Log in to *VM* and execute  
        `VM$ mtr rtfm.mit.edu -n`
    
    2.  Let it run for a few seconds. Look at the Avg-column. There is
        significant delay when communicating over long physical
        distances. *VM* is physically located on a server in Gjøvik. Try
        to guess which IP-addresses is involved and how much delay in ms
        (milliseconds) there is between
        
        1.  Gjøvik to Trondheim
        
        2.  Trondheim to Oslo
        
        3.  Oslo to Europe
        
        4.  Europe to USA
    
    3.  (type `q` to quit) Now execute without the `-n` and see if your
        guesses where correct based on the hostnames  
        `VM$ mtr rtfm.mit.edu`

2.  **Local, Private and Public IP addresses**.
    
    1.  Open two terminals and log in to each host, respectively.
    
    2.  Run the following command on both hosts to see which IP
        addresses they have  
        `ip --brief a`
    
    You will see three different types of IP addresses. One is the
    normal public addresses that you can reach over the internet, the
    two others you will find in the table . Do you understand why *VM*
    needs a floating ip address? What type of address is a floating ip
    address? Ask teacher to explain if you are unsure. Btw, if wonder
    what ip address your server is showing to the outside world, you can
    ask the very cool service
    [icanhazip](https://major.io/icanhazip-com-faq), e.g.  
    `curl icanhazip.com`  
    `curl -4 https://icanhazip.com`  
    `curl -6 https://icanhazip.com`  
    `curl https://icanhazptr.com`

3.  **Virtual Private Networking (VPN)**. Why do you get different
    access to network services if you use this magical thing "VPN"?
    
    1.  Do this when you are NOT in a NTNU network, meaning do this from
        home or share WiFi from your mobile phone and connect to that
        instead of Eduroam.
    
    2.  Check your routing table before and after connecting to VPN  
        Windows (PowerShell): `Get-NetRoute`  
        Mac: `route -n`  
        Linux: `ip r`

4.  **Checking connection with ping**.
    
    1.  Open two terminals and log in to each host, respectively.
    
    2.  Ping *login* from *VM*  
        `VM$ ping login.stud.ntnu.no`
    
    3.  Ping the IP address of *login* from *VM*  
        `VM$ ping IP_ADDRESS_OF_LOGIN`
    
    4.  Ping *VM* from *login*  
        `login$ ping FLOATING_IP_ADDRESS_OF_VM`
    
    This is the basic test for connectivity between two hosts.

5.  **Ports and sockets**. A port is a number between 0 and 65535
    (\(2^{16}-1\)). A socket is the actual implementation of a end
    point, meaning its defined with a port number, an IP address and a
    protocol (commonly TCP or UDP). There are other kinds of sockets as
    well, but we only care about TCP and UDP sockets. For a DNS record
    to be sent or received it had to leave or arrive to or from a host
    through a socket. E.g. a socket used for DNS uses UDP on port 53 (a
    DNS server will receive and return DNS records from port 53, while
    the client contacting the server uses some port higher than 1023 on
    the client) . Let’s look closely at a simple TCP socket with a
    client and a server program. The example we will use is borrowed
    from .
    
    1.  Open two terminals and log in to each host, respectively.
    
    2.  To see which ports are open and listening  
        `ss -tlpn`  
        You can check if any process is listening on port 8850  
        `ss -tlpn | grep 8850`
    
    3.  Download a simple server on *VM* and a simple client on
        *login*  
        `VM$ wget https://folk.ntnu.no/erikhje/echo-server.py`  
        `login$ wget https://folk.ntnu.no/erikhje/echo-client.py`
    
    4.  Read carefully through both client and server and see if you
        understand what is going to happen when we execute them.
    
    5.  On *VM*, edit the file `echo-server.py` you just download and
        change the ’HOST’ to the IP address of *VM* (not the floating
        ip\!) and the ’PORT’ to 8850. Make the file executable, run it
        in the background and see that it is listening on port 8850  
        `VM$ chmod +x echo-server.py`  
        `VM$ ./echo-server.py &`  
        `VM$ ss -tlpn | grep 8850`
    
    6.  On *login*, edit the file `echo-client.py` you just download and
        change the ’HOST’ to the FLOATING IP address of *VM* and the
        ’PORT’ to 8850. Make the file executable, run it  
        `login$ chmod +x echo-client.py`  
        `login$ ./echo-client.py &`
    
    7.  Did you get any output on both sides? Was it what you expected
        based on reading the contents of the files?

# IPv4, DNS, TCP/UDP and Cryptography

## DNS

[Internet Corporation for Assigned Names and Numbers
(ICANN)](https://www.icann.org)

[How DNS works](https://howdns.works)

  - Delay solved with caching

  - Host aliasing

  - Mail server aliasing

  - Load distribution

  - Distributed hierarchical database

  - Resource records  
    Name, Value, Type, TTL

  - UDP  
    (TCP in special cases)

  - 16-bit ID field  
    can you guess it to “poison a cache”?

##### Interaction of DNS servers

![image](img/dns-resolve.png)

##### Several Caches Involved

![image](img/dns-caches.png)

## IP

  - 32 bits  
    `11000010 00111111 11111000 00101111`  
    binary number commonly written with `0b` in front:  
    `0b11000010 00111111 11111000 00101111`  
    (hexadecimal this would be `C2 3F F8 2F`,  
    commonly written `0xC23FF82F`)

  - Dotted decimal  
    `194.63.248.47` (DNS: www.sau.no)

  - Subnet, Netmask, CIDR  
    How many IPs do you have on a `x.x.x.x/n` subnet? \(2^{32-n}\)

  - Network prefix  
    “The most significant bits”

  - Address aggregation

There is a nice tool called `ipcalc` (a similar tool is [available
online](https://ipcalc.me/)).

    $ sudo apt install ipcalc
    
    $ ipcalc 192.168.2.0/23
    Address:   192.168.2.0          11000000.10101000.0000001 0.00000000
    Netmask:   255.255.254.0 = 23   11111111.11111111.1111111 0.00000000
    Wildcard:  0.0.1.255            00000000.00000000.0000000 1.11111111
    =>
    Network:   192.168.2.0/23       11000000.10101000.0000001 0.00000000
    HostMin:   192.168.2.1          11000000.10101000.0000001 0.00000001
    HostMax:   192.168.3.254        11000000.10101000.0000001 1.11111110
    Broadcast: 192.168.3.255        11000000.10101000.0000001 1.11111111
    Hosts/Net: 510                   Class C, Private Internet
    
    $ 

## DHCP

  - SOLVES THE ADDRESS ASSIGNMENT PROBLEM  
    allows dynamically assigned addresses instead of static addresses

  - Four-way dialogue  
    DHCP discover, DHCP offer, DHCP request, DHCP ACK

## NAT

  - RFC1918  
    Addresses that are not used on the Internet: `10.0.0.0/8`,
    `172.16.0.0/12` and `192.168.0.0/16`

  - NAT-enabled router  
    offers these addresses internally and translates them using a
    NAT-table when forwarding packets onto the Internet.

  - Security  
    NAT is not a security mechanism, makes it difficult to reach hosts
    on the internal network, but not impossible.

## Cryptography

  - Information security  
    CIA (Confidentiality, Integrity, Availability), Identification,
    Authentication, Authorization

  - Secret key  
    you can think of as some kind of long unreadable password

  - Symmetric encryption/decryption

  - Public-key cryptography (asymmetric encryption/decryption)  
    Instead of a secret key, you have a key pair: a secret private key
    and a public key

  - Message digest  
    a cryptographic hash function (one-way, fingerprint)

  - Digital signature  
    a message digest encrypted with the private key (so others can
    verify it using the corresponding public key)

  - Certificate  
    is a public key with some extra information, and is digitally signed
    (with some other public key)

  - Certificate authority (CA)  
    a public key/certificate used to signed others

  - Root certificate  
    a top-level certificate, self-signed, typically trusted by all
    parties involved in the communication

## Review questions and problems

1.  What is included in a DNS resource record? Choose four of these.
    
    1.  Connection speed.
    
    2.  TTL.
    
    3.  Routing.
    
    4.  Name.
    
    5.  Type.
    
    6.  Value.
    
    7.  Fragments.
    
    8.  Port 443.
    
    9.  Congestion information.
    
    10. Root DNS servers.

2.  How many IP-addresses do you have on a /28 subnet?
    
    1.  16\.
    
    2.  8\.
    
    3.  256\.
    
    4.  64\.

3.  Which IP-address is in the subnet `223.1.2.0/23`?
    
    1.  `223.2.1.60`.
    
    2.  `223.1.3.124`.
    
    3.  `223.1.1.124`.
    
    4.  `223.1.4.60`.

4.  Why does DHCP use a four-way dialogue instead of just a two-way
    dialogue?
    
    1.  Because a host might get more than one DHCP offer message.
    
    2.  Because a host also needs to be assigned a DNS name.
    
    3.  Because a host needs double confirmation due to DHCP using
        unreliable UDP.
    
    4.  Because a host performs the dialogue over both TCP and UDP.

5.  Explain how a DNS lookup works. Use `dig rtfm.mit.edu` as an example
    and assume nothing is cached by any of the involved DNS servers.

6.  Using only public key cryptography, explain how Alice can encrypt a
    file, send it to Bob, and Bob can decrypt it.

## Lab tutorials

1.  **Binary, octal, decimal and hexadecimal numbers**. Google has a
    builtin calculator for converting between number systems, try the
    following google searches
    
        194 in binary
        0b11000010 in octal
        0b11000010 in hex
        0o302 in hex
        0xC2 in decimal
        # note: 0b is binary, 0o is octal, 0x is hexadecimal
    
    Hexadecimal numbers are the numbers `0123456789ABCDEF`, 16 numbers
    in total, and note that \(2^4=16\). Why use hexadecimal numbers (and
    on some rare occasions octal numbers)? Because a computer uses
    binary numbers and a hexadecimal number is a nice compact
    representation of four binary digits (octal represents three). There
    are eight bits in one Byte, so a Byte can be written as two
    hexadecimal numbers (e.g. the example above where the Byte
    `11000010` can be written as `C2`).

2.  **DNS (Domain Name System)**. DNS works by sending messages called
    Resource Records in UDP packets. It uses UDP since it needs to be
    lightweight and simple. If a packet is lost, we can just try again.
    However, there are security and privacy concerns  over DNS and
    implementations of DNS over HTTPS are being deployed and seems to
    work without too big performance hit .
    
    1.  Open two terminals and log in to each host, respectively.
    
    2.  Which DNS server(s) does *login* use (these are probably NTNUs
        own DNS servers)?  
        `login$ cat /etc/resolv.conf`
    
    3.  Which DNS server(s) does *VM* use?  
        `VM$ cat /etc/resolv.conf`  
        `VM$ systemd-resolve --status` (scroll to see ’DNS Servers’)
    
    4.  Do a DNS lookup to find the IP address of `rtfm.mit.edu`  
        `dig rtfm.mit.edu`  
        The CNAME resource record represents an alias. It means it is a
        host name that maps to another host name. The A resource record
        is what we are really looking for. It contains the host name to
        IP address mapping.
    
    5.  Do a reverse DNS lookup to find the host name of `18.49.2.75`  
        `dig -x 18.49.2.75`  
        The PTR resource record contains the reverse mapping. Note that
        some hosts might have A records but not PTR records, meaning a
        host name to IP address lookup will work, but not the other way
        around.
    
    6.  From *VM* skip the internal DNS server and ask two other servers
        for the lookup instead: first one of NTNUs servers, then one of
        Google’s public servers.  
        `VM$ dig @129.241.0.200 rtfm.mit.edu`  
        `VM$ dig @8.8.4.4 rtfm.mit.edu`  
        In the ANSWER section from the dig command, make sure you
        understand the TTL (Time To Live) number in the second column
        (1795 in the example below). Run one of the commands repeatedly
        and see how the number changes.
        
            ;; ANSWER SECTION:
            rtfm.mit.edu.           1795    IN      CNAME   xvm-75.mit.edu.
            xvm-75.mit.edu.         1795    IN      A       18.49.2.75
    
    7.  Observe the entire hierarchy of servers involved in a fresh DNS
        lookup (here you will also see other resource records: the NS
        (Name Server) record used to delegate responsibility for a
        domain to another DNS server, and the RRSIG which contains a key
        used to attempt to add security to the DNS system)  
        `dig @129.241.0.200 +trace rtfm.mit.edu`  
        Notice that some servers (root name servers) are responsible for
        the top level called just `.`, some servers are responsible for
        `edu.` and some servers are responsible for `mit.edu.` and one
        of them (the ones responsible for `mit.edu.`) will return the IP
        address of `rtfm.mit.edu`. *Notice that it is not very often
        that we ask the root name servers since there is caching used at
        all levels (temporary storage for a time indicated by the TTL
        (Time To Live) value).* Do you know what caching is? Ask teacher
        if unsure.

3.  **Basic cryptography**. If you have a file you want to protect
    (either for transmission or just for storage, or both) you should
    use cryptography. Using cryptography means using some kind of
    cryptographic algorithms that is implemented in the software you
    will use. *Make sure you only use cryptography-software that is
    trustworthy: it should use standard and well-known cryptographic
    algorithms.* If you just want a simple interface for a file you are
    sending to someone (e.g. when your teacher is sending an exam to an
    external for review) one good option is 7-zip (on Linux the package
    is called `p7zip`) . In the exercises we are going to do, we want to
    control details and have great flexibility so we will use OpenSSL 
    which is installed by default on Linux (install with `brew` on Mac)
    and you can install in PowerShell on Windows with (you do not have
    to do this now, this is just in case you would like to use OpenSSL
    on Windows)
    
        # PowerShell as administrator:
        Install-PackageProvider Chocolatey
        # Do not have to be administrator for the rest:
        # (but maybe need to restart PowerShell)
        Install-Package OpenSSL.Light
        # where did it go?
        Get-Package OpenSSL.Light
        # run the actual installation of OpenSSL
        C:\Chocolatey\lib\OpenSSL.Light.1.1.1.20181020\
         tools\Win64OpenSSL_Light-1_1_1.exe
        # Add OpenSSL to the PATH by adding C:\Program Files\OpenSSL-Win64\bin\ 
        # to the environment variable $env:path in the file you open with
        notepad $profile
        # e.g. this file might look like this:
        # $env:Path += "C:\Chocolatey\bin;C:\Program Files\OpenSSL-Win64\bin\"
        # restart PowerShell and check that you can run openssl commands
        openssl version
    
    1.  Log in to one of your hosts and create a file `secret.txt` with
        a secret message `mysil` (feel free to create your own secret
        message)  
        `echo mysil > secret.txt`
    
    2.  *Symmetric encryption/decryption*. Symmetric cryptography is
        fast (often implemented in hardware).
        
        1.  Encrypt the file with 256-bit AES (the same algorithm used
            by the US Government )  
            `openssl enc -aes-256-cbc -a -pbkdf2 \`  
            `  -in secret.txt -out secret.txt.enc `  
            Email the encrypted file to one of your fellow students (or
            to yourself). How can you transfer the password to they?
        
        2.  The file you received you can decrypt with  
            `openssl enc -aes-256-cbc -a -pbkdf2 \`  
            `  -d -in secret.txt.enc -out secret.txt `
        
        3.  The command line option `-a` is for Base64 encoding which is
            necessary for protecting the file during transfer over e.g.
            email which is designed originally just for transfer of text
            and not arbitrary binary files. What is the command line
            option `-pbkdf2`? Read the entry in the man page  
            `man openssl-enc`  
            Search the internet for `PBKDF2` to learn what is behind the
            acronym.
    
    3.  *Public-key cryptography (asymmetric encryption/decryption)*.
        Public-key cryptography is slow, but it solves the problem of
        transferring a secret key: you do not have to do that anymore\!
        (but since it is slow we just use it as a first step to
        transfer/agree on a secret which then can be used for symmetric
        cryptography).
        
        1.  Create a RSA 2048-bit key pair  
            `openssl genpkey -algorithm RSA -out key.pem \`  
            `  -pkeyopt rsa_keygen_bits:2048 `  
            Extract the public key from the private key (for practical
            reasons the public key is commonly included in the private
            key)  
            `openssl rsa -in key.pem -pubout -out pub-key.pem`  
            Your private key is `key.pem` and your public key is
            `pub-key.pem`
        
        2.  Encrypt a file with the public key (note: this only works
            for small files), decrypt it with the private key (this is
            how someone can send you a secret without transferring a
            secret key/password, they only need your public key which of
            course is public)  
            `openssl pkeyutl -encrypt -in secret.txt -pubin \`  
            `  -inkey pub-key.pem -out secret.txt.enc `  
            `openssl pkeyutl -decrypt -in secret.txt.enc -inkey key.pem`
        
        3.  If we want to encrypt a file with the private key, this is
            called signing the file. We do not decrypt the encrypted
            file (the signature) but we verify with the public key that
            the encrypted file was generated from the source file (this
            is how someone can verify that the file they received is
            from you since you are the only one in possession of the
            private key)  
            `openssl pkeyutl -sign -in secret.txt -inkey key.pem -out
            sig`  
            `openssl pkeyutl -verify -sigfile sig -in secret.txt -pubin
            \`  
            `  -inkey pub-key.pem `
    
    4.  *Message digest and digital signature*. Doing public-key
        cryptography on large files is slow, so when creating digital
        signatures we first create a message digest (a “fingerprint” of
        the file, commonly a hash function) from the file, and then
        encrypt the message digest with our private key.
        
        1.  Create a message digest with the default SHA256 hash
            algorithm  
            `openssl dgst secret.txt`
        
        2.  Create a message digest and sign it with your private key  
            `openssl dgst -sign key.pem -out sig secret.txt`
        
        3.  Verify the signature with your public key  
            `openssl dgst -verify pub-key.pem -signature sig secret.txt`
    
    5.  *Adding trust: a certificate authority*. We now know that it all
        begins with the public key. If we have a public key we can
        establish a shared secret for use with fast symmetric
        cryptography, and we can verify digital signatures. But how can
        we trust the public key: how do we know which host the public
        key belong to? We can just accept that a hosts tells us that
        “this is my public key, just believe me”. We need a mechanism
        with a “trusted third party”, which we call a certificate
        authority that we use to verify public keys so we actually can
        trust the public key we receive from a host. A certificate
        authority is represented in our host through a trusted
        certificate. A certificate is just a public key together with
        some extra information. Your host contains many of these trusted
        certificates in many location, see the figure "Root Certificate
        Stores" .
        
        1.  Generate a new private key. Since the corresponding public
            key will be a trusted root certificate, the private key is
            extra important so this time we set a password on it  
            `openssl genpkey -algorithm RSA -out myCA.key \`  
            `  -pkeyopt rsa_keygen_bits:2048 -aes-256-cbc `
        
        2.  Create the root certificate
            
                # Note: openssl might have an "outdated" option in its config file
                # that will give you unnecessary error messages, update the config
                # file with this command before moving on (note: this is of course 
                # only possible to do on VM where you can be root, you are not 
                # allowed to be root on login since it is a shared server):
                sudo sed -i '/RANDFILE/d' /etc/ssl/openssl.cnf
                
                openssl req \
                    -x509 \
                    -nodes \
                    -new \
                    -key myCA.key \
                    -out myCA.pem \
                    -config <(cat /etc/ssl/openssl.cnf) \
                    -subj /CN=MyCA \
                    -sha256 \
                    -days 3650
        
        3.  A certificate signing request (CSR) consists mainly of the
            public key of a key pair, and some additional information.
            Both of these components are inserted into the certificate
            when it is signed. Create a CSR from your public key
            `pub-key.pem` and sign it with your root certificate’s
            private key `myCA.key` (meaning anyone who have and trust
            your root certificate `myCA.pem` can verify the signature)
            
                openssl req \
                    -new \
                    -key key.pem \
                    -out pub-key.csr \
                    -subj /CN=FLOATING_IP_OF_VM \
                    -reqexts SAN \
                    -extensions SAN \
                    -config <(cat /etc/ssl/openssl.cnf \
                        <(printf '[SAN]\nsubjectAltName=IP:FLOATING_IP_OF_VM'))
                
                openssl x509 \
                    -req \
                    -in pub-key.csr \
                    -CA myCA.pem \
                    -CAkey myCA.key \
                    -CAcreateserial \
                    -out pub-key.crt \
                    -days 1825 \
                    -sha256 \
                    -extensions v3_req \
                    -extfile <(cat /etc/ssl/openssl.cnf \
                        <(printf '[SAN]\nsubjectAltName=IP:FLOATING_IP_OF_VM'))
            
            View the contents of the certificate you created  
            `cat pub-key.crt`  
            `openssl x509 -text -noout -in pub-key.crt`
        
        4.  Use the root certificate to verify the signature of the
            certificate  
            `openssl verify -CAfile myCA.pem pub-key.crt`
            
            I can now create new certificates for new services and
            anyone that have the “trusted root certificate” `myCA.pem`
            can be able to verify that the new certificates they are
            presented with from my services (e.g. new websites) come
            from me.
        
        5.  Btw, you are using ssh, where are the public and private
            keys your ssh session is using? When did you establish trust
            (since no CA is involved in ssh)?
    
    *Note: all software have bugs from time to time, some are serious,
    some are not. Even an extremely widely used library and toolkit like
    OpenSSL had a serious bug between 2012 and 2014: Heartbleed *

# SMTP, HTTP/HTTPS, TLS/SSL

## SMTP

  - SMTP  
    Simple Mail Transfer Protocol

  - Ports  
    TCP, 25 (and 587)

  - Protocol vs Mail message format  
    Notice in the exercise how the actual content of the email message
    (including From, To, CC, Date, Subject) is NOT a part of the SMTP
    protocol.

## HTTP/HTTPS

  - HTTP(S)  
    HyperText Transfer Protocol (Secure)

  - SSL/TLS  
    Secure Sockets Layer / Transport Layer Security, it is the protocol
    that provides the ’S’ in many other protocols

  - Ports  
    TCP, 80 (443 for HTTPS)

  - Headers and message body

  - Host in GET-request

  - nginx

  - self-signed certificate

  - certificate store

  - certificate authority (CA)

## Review questions and problems

1.  What is the meaning of the `-I` option in the command `curl -I
    ntnu.no` ?
    
    1.  Ignore upper/lower-case.
    
    2.  Show only the response headers.
    
    3.  Recursively show all sub-pages.
    
    4.  Include all cookies.

2.  What does the command `ss -tlpn` (or `netstat -tlpn`) do?
    
    1.  Shows all native TLS protocols.
    
    2.  Shows all DNS transactions IDs.
    
    3.  Shows all HTTPS request headers.
    
    4.  Shows all listening TCP sockets.

3.  When you do a HTTP GET-request to a server, why do you have to
    include the ’Host:’ header?

4.  When you visit a website which has a self-signed certificate or a
    certificate signed by an unknown certificate authority you get a
    warning in your webbrowser. How do you make this website an approved
    website so you dont get a warning?

5.  When visiting a website, what is the difference between visiting it
    using port 80 vs port 443?

6.  HTTPS provides privacy, integrity and identification. What does this
    mean?

## Lab tutorials

1.  **Simple Mail Transfer Protocol (SMTP)**. You are now going to
    pretend to be a mail client sending an email. If you were to program
    a mail client, this is what the program needs to do when it talks
    SMTP application layer protocol to the SMTP-server.
    
    1.  Log in to `login`.
    
    2.  Establish a tcp-connection to port 25 on NTNUs mail server  
        `login$ telnet mx.ntnu.no 25`
    
    3.  Send an email to one of your fellow students by doing the dialog
        at  but replace:
        
          - `relay.example.com` with `login.stud.ntnu.no`
        
          - `bob@example.com` and `theboss@example.com` with your NTNU
            email address
        
          - `alice@example.com` with one of your fellow students NTNU
            email address
    
    4.  Btw, DNS is also the reason you can send email to
        `username@gmail.com` and you do not have to specify the email
        address  
        `username@gmail-smtp-in.l.google.com`  
        This is because the MX-record in DNS specifies which server(s)
        receive email for a domain. Do a DNS lookup for the MX-record of
        Gmail and NTNU to confirm this  
        `dig gmail.com MX`  
        `dig ntnu.no MX`  

2.  **HTTP and HTTPS**. Hyper Text Transfer Protocol (HTTP) without
    TLS/SSL is listening on TCP port 80. HTTP with TLS/SSL is listening
    on TCP port 443.
    
    1.  Log in to one of your hosts.
    
    2.  Talk HTTP with the website <http://www.gossin.no> to see that it
        returns a HTML web page  
        
            telnet www.gossin.no 80
            GET / HTTP/1.1
            Host: gossin.no
            # hit enter twice
        
        you can also use netcat
        
            nc -C www.gossin.no 80
            GET / HTTP/1.1
            Host: gossin.no
            # hit enter twice
    
    3.  This website offers HTTPS as well so lets do that also
        
            openssl s_client -crlf -connect www.gossin.no:443
            GET / HTTP/1.1
            Host: gossin.no
            # hit enter twice
        
        you can also use `ncat`
        
            ncat -C --ssl www.gossin.no 443
            GET / HTTP/1.1
            Host: gossin.no
            # hit enter twice

3.  **HTTP requests with curl**. `curl` is similar to `wget`. Generally
    you use `wget` to download files, and `curl` to do more specific
    HTTP-requests (`curl` also supports other protocols). You can read
    more about their differences at .
    
    ![From
    <https://twitter.com/b0rk/status/1159839824594915335?s=20><span label="Bork:HTTP"></span>](img/http-requests.jpg)
    
    Do all the exercises in figure [6.1](#Bork:HTTP).

4.  **Webserver with HTTPS**. All websites should use HTTPS . Let’s do
    this exercise in three steps: install simplest HTTP webserver first,
    then move to HTTPS with a “self-signed certificate” (often used just
    for testing), then finally using the “certificate authority” and
    certificates we created in the previous exercise to do the best we
    can in our situation. In a production/real-world situation we would
    have a domain name and a public reachable IP address (we would not
    have to use VPN when outside NTNU) and we would use an existing
    certificate authority and just get a certificate from e.g. Let’s
    Encrypt .
    
    1.  *A simple HTTP webserver*. Log in to *VM* and install and
        configure a simple HTTP webserver (we will use the webserver
        nginx , pronounced “Engine X”)
        
        1.  Which tcp ports are open and listening before install?  
            `sudo ss -tlpn`
        
        2.  Install nginx and check open ports again. Nginx should begin
            listening on port 80 all interfaces by default. Notice also
            that you see the process name and process ID of nginx.
            
                sudo apt update
                sudo apt install nginx
                sudo ss -tlpn 
        
        3.  Let’s look briefly at how this webserver works. Where is the
            configuration for the website (meaning: where is it defined
            that it should listen on port 80, from which directory
            should it serve webpages, etc)? How is it enabled? Where is
            the HTML-code for the default webpage?
            
                ls -l /etc/nginx/sites-enabled/ 
                # default site enabled by symbolic link
                cat /etc/nginx/sites-enabled/default
                # show actual config (meaning: show me all lines that are 
                # not comments), note there is no IP or DNS info here
                grep -Pv '^\s*#' /etc/nginx/sites-enabled/default
                # see default HTML-code
                cat /var/www/html/index.nginx-debian.html
        
        4.  Visit the floating ip in browser and see the default web
            page.
    
    2.  *A simple HTTPS webserver with a self-signed certificate*.
        Reconfigure the webserver to use HTTPS with self-signed
        certificate. A self-signed certificate means that the public key
        (and its additional information) is signed with its own private
        key.
        
        1.  Create a self-signed certificate  and copy the self-signed
            certificate and its corresponding private key to its “proper
            place”.
            
                openssl req \
                    -newkey rsa:2048 \
                    -x509 \
                    -nodes \
                    -keyout server.key \
                    -new \
                    -out server.crt \
                    -subj /CN=FLOATING_IP_OF_VM \
                    -reqexts SAN \
                    -extensions SAN \
                    -config <(cat /etc/ssl/openssl.cnf \
                        <(printf '[SAN]\nsubjectAltName=IP:FLOATING_IP_OF_VM')) \
                    -sha256 \
                    -days 1825
                sudo cp server.crt /etc/ssl/certs/nginx.crt
                sudo cp server.key /etc/ssl/private/nginx.key
        
        2.  Create a new website by creating the file  
            `/etc/nginx/sites-available/FLOATING_IP_OF_VM`  
            with the contents
            
                server {
                    listen 443 ssl;
                    listen [::]:443 ssl;
                
                    server_name FLOATING_IP_OF_VM;
                    ssl_certificate /etc/ssl/certs/nginx.crt;
                    ssl_certificate_key /etc/ssl/private/nginx.key;
                
                    root /var/www/html;
                    index index.html index.htm index.nginx-debian.html;
                }
                
                server {
                    listen 80;
                    listen [::]:80;
                
                    server_name FLOATING_IP_OF_VM;
                
                    return 302 https://$server_name$request_uri;
                }
            
            enable it with  
            `cd /etc/nginx/sites-enabled`  
            `ln -s /etc/nginx/sites-available/FLOATING_IP_OF_VM .`
        
        3.  Restart nginx  
            `sudo systemctl restart nginx`  
            and visit the floating ip in your browser to see that you
            get a warning. Do not add exception or do anything, just
            leave the website for now.
        
        4.  Add the self-signed certificate to a certificate store that
            your browser uses. This works for Chrome on Windows or Linux
            (for Mac, google something like “add certificate to macOS
            keychain”). From : "Google Chrome attempts to use the root
            certificate store of the underlying operating system to
            determine whether an SSL certificate presented by a site is
            indeed trustworthy, with a few exceptions".
            
                # Linux, list locally installed certificates, probably empty
                certutil -L -d sql:/home/ubuntu/.pki/nssdb
                certutil -d sql:/home/ubuntu/.pki/nssdb -A -t "P,," \
                 -n "FLOATING_IP_OF_VM" -i server.crt
                
                # Chrome or Internet Explorer on Windows, 
                # PowerShell as administrator
                ls Cert:\LocalMachine\Root\
                Import-Certificate -CertStoreLocation Cert:\LocalMachine\Root `
                 -FilePath .\server.crt
                
                # Firefox: Preferences, Privacy and Security, Certificates,
                #  View Certificates, Import Certificate
        
        5.  Restart Chrome, press `F5` to reload (you can also press F12
            to open Chrome Dev Tools and right-click the refresh button
            to get the option “Empty cache and hard reload” but a normal
            F5 should be enough), you should now be able to visit the
            website without warnings.
    
    3.  *A simple HTTPS webserver with our own Certificate Authority*.
        Reconfigure the webserver to use HTTPS with a Certificate
        Authority (a trusted root certificate).
        
        1.  Replace the self-signed certificate and its private key with
            the CA-signed keys from the earlier exercise  
            `sudo cp pub-key.crt /etc/ssl/certs/nginx.crt`  
            `sudo cp key.pem /etc/ssl/private/nginx.key`
        
        2.  Restart nginx  
            `sudo systemctl restart nginx`  
            and visit the floating ip in your browser to see that you
            get a warning. Do not add exception or do anything, just
            leave the website for now.
        
        3.  Add the root certificate `myCA.pem` (maybe needs to be
            renamed to `myCA.crt` on Windows) to a certificate store
            that your browser uses.
            
                # Linux, list your locally installed certificate
                certutil -L -d sql:/home/ubuntu/.pki/nssdb
                certutil -d sql:/home/ubuntu/.pki/nssdb -A -t "C,," \
                 -n "myCA" -i myCA.pem
                
                # Chrome on Windows, PowerShell as administrator
                ls Cert:\LocalMachine\Root\
                Import-Certificate -CertStoreLocation Cert:\LocalMachine\Root `
                 -FilePath .\myCA.crt
        
        4.  NOTE: THIS DOES NOT WORK ON CHROME YET SINCE CHROME HAS SOME
            CERTIFICATE REQUIREMENT THAT WE HAVEN’T FIGURED OUT, BUT IT
            WORKS ON FIREFOX AND INTERNET EXPLORER. Restart Chrome,
            press `F5` to reload (you can also press F12 to open Chrome
            Dev Tools and right-click the refresh button to get the
            option “Empty cache and hard reload” but a normal F5 should
            be enough), you should now be able to visit the website
            without warnings.
    
    4.  *Test security and configuration of a website*. You can do this
        on public reachable websites with tools like
        
          - [SSL Server Test](https://www.ssllabs.com/ssltest)
        
          - [Security Headers](https://securityheaders.com)
        
        On non-public reachable websites like our own, we can use tools
        like SSLyze  from OWASP-lists .
    
    *Whenever we develop/create an IT-system, we should test it\!*

5.  **HTTPS**. Do we understand how cryptography and the SSL/TLS
    protocol is used with HTTP to create HTTPS? Read the entire comic
    (all five episodes) at [How HTTPS works](https://howhttps.works) to
    make sure you are in control.

6.  **Real-world usage**. In the real world you would have your own
    domain and public reachable IP address. To launch a website you
    should always have it support HTTPS and probably force clients to
    use HTTPS when connecting to it. You now know that you need a
    certificate signed from a CA. Today the process of getting a signed
    certificate from a CA can be automated if you use Let’s Encrypt  as
    your CA. How can Let’s Encrypt know that you are who you claim to be
    such that it can sign your public key? Read how this works at [How
    It Works](https://letsencrypt.org/how-it-works) (read carefully the
    short paragraph “Domain Validation”).
